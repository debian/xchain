#!/bin/sh
# Syntax: install.sh roodir
# Where rootdir is the directory that all paths are relative to.
# If no rootdir is supplied it will be taken to be /
# This will be / for most people. It is changeable for the
# benefit of dpkg-buildpackage

set -e

[ -f xchain.6 ] && gzip -9 xchain.6
# [ -f xchain.6.gz ] && gunzip xchain.6.gz

# comment out one of the above lines depending on whether your
# system supports gzipped man pages.

bindir=$1/usr/games
mandir=$1/usr/man/man6
menudir=$1/usr/lib/menu
docdir=$1/usr/doc/xchain

[ -d $bindir ] || mkdir -m 0755 -p $bindir
chmod 0755 xchain
cp xchain $bindir

[ -d $mandir ] || mkdir -m 0755 -p $mandir
[ -f xchain.6 ] && chmod 0644 xchain.6
[ -f xchain.6 ] && cp xchain.6 $mandir
[ -f xchain.6.gz ] && chmod 0644 xchain.6.gz
[ -f xchain.6.gz ] && cp xchain.6.gz $mandir

[ -d $menudir ] || mkdir -m 0755 -p $menudir
[ -f xchain.menu ] && chmod 0644 xchain.menu
[ -f xchain.menu ] && cp xchain.menu ${menudir}/xchain

[ -d $docdir ] || mkdir -m 0755 -p $docdir
[ -f README ] && chmod 0644 README
[ -f README ] && cp README $docdir
[ -f copyright ] && chmod 0644 copyright
[ -f copyright ] && cp copyright $docdir
