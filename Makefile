DESTDIR = /
# This is so dpkg-buildpackage can build in the correct dir

CC=gcc
CFLAGS=-O2 -Wall

OBJS=		xchain.o

.c.o:
		$(CC) -c $(CFLAGS) $<

all:		xchain

chain:	$(OBJS)
		$(CC) -o xchain $(CFLAGS) $(OBJS)
		chmod 0755 xchain

install:	xchain
		./install.sh $(DESTDIR)

clean:
		rm -f *.o *~ core *.bak ./xchain ./build
